import {BrowserRouter as Router, Route} from "react-router-dom";
import './App.css';
import Body from './components/body';
import Footer from './components/footer';
import Contact from './components/contact';
import ScrollToTop from './components/ScrollToTop';

function App() {
  
  return (
    <Router>
      <div className="body-style"></div>
      <ScrollToTop /> 
      <Route path='/' exact> <Body/> <Footer/></Route>
      <Route path='/contact'> <Contact /> <Footer /></Route>
    </Router> 

  );
}

export default App;
