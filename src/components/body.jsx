import React, { Component, useEffect } from "react";
import { Card } from "primereact/card";
import { Button } from "primereact/button";
import LinkButton from "./LinkButton";

/*const elements = {
  elementOne: "/image-container/low-element.png",
  elementTwo: "/image-container/top-element.png",
  elementThree: "/image-container/sub-top.png",
};*/

const core = {
  Title: "CORE VALUES",
};

const tech = {
  Title: "OUR EXPERTISE",
};

const projImages = {
  one: "https://dummyimage.com/hd1080",
  two: "https://dummyimage.com/hd1080",
  three: "https://dummyimage.com/hd1080",
};

const cardHeader = (
  //<i class="fi-rr-layout-fluid icon-style" style={{ fontSize: 105 }}></i>
  <img className="card-image-style" src="./image-container/icon3.png"></img>
);

const cardHeaderTwo = (
  //<i class="fi-rr-browser icon-style" style={{ fontSize: 105 }}></i>
  <img className="card-image-style" src="./image-container/icon2.png"></img>
);
const cardHeaderThree = (
  //<i class="fi-rr-shield-check icon-style" style={{ fontSize: 105 }}></i>
  <img className="card-image-style" src="./image-container/icon1.png"></img>
);

const projImage = (
  <img alt="Card" src={projImages.one} className="works-image-style" />
);

const projImageTwo = (
  <img alt="Card" src={projImages.two} className="works-image-style" />
);

const projImageThree = (
  <img alt="Card" src={projImages.three} className="works-image-style" />
);

const companyContents = {
  companyName: "DARKSPOT",
  subCompanyName: "IT SOLUTIONS",
  companyTag: "We make apps!",
};

const aboutContents = {
  title: "ABOUT US",
  content:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu vitae elementum curabitur vitae nunc sed velit. Id volutpat lacus laoreet non curabitur gravida arcu ac tortor. Tellus in hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Dis parturient montes nascetur ridiculus mus mauris vitae. Tellus elementum sagittis vitae et leo duis ut diam quam. Aliquam ultrices sagittis orci a. Convallis posuere morbi leo urna molestie at elementum eu. Non enim praesent elementum facilisis leo vel fringilla.",
};

const servicesContent = {
  title: "SERVICES",
  cardContentOne:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  cardContentTwo:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  cardContentThree:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
};

const projectContent = {
  title: "RECENT WORKS",
  projectTitle: "Project Title",
  projectDesc:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  projectTitleTwo: "Project Title",
  projectDescTwo:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  projectTitleThree: "Project Title",
  projectDescThree:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
};

const contactContent = {
  title: "CONTACT US",
  content: "Would you like to work with us?",
  subContent: " Awesome!",
};

class Body extends Component {
  state = {};

  headerText = {
    fontSize: 40,
    paddingTop: 100,
  };

  headerTextTwo = {
    fontSize: 40,
    paddingTop: 80,
  };

  titleText = {
    fontSize: 28,
  };

  contactText = {
    fontSize: 30,
  };

  render() {
    return (
      <React.Fragment>
        <div className="company-header">
          {/**<img src="./image-container/low-element.png" className="element-image-style-one"></img>
          <img src="./image-container/top-element.png" className="element-image-style-two"></img>
    <img src="./image-container/sub-top.png" className="element-sub-image-style"></img>**/}
          <div className="p-fluid p-grid p-d-flex p-flex-column p-flex-md-row">
          <div className="p-field p-col-12 p-md-4">
          <img
             data-aos="fade-right"
             data-aos-offset="200"
             data-aos-delay="50"
             data-aos-duration="1200"
             data-aos-mirror="true"
            src="./image-container/bulb-svg.svg"
            className="element-image-style-bulb"
          ></img>
          </div>
        
            {/**<div className="p-field p-col-12 p-md-8">
              <motion.h1
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                transition={{ delay: 0.5, duration: 2 }}
                className="highlight-font theme-dark-blue p-text-right company-text-style p-text-bold"
              >
                {companyContents.companyName} <br />
                {companyContents.subCompanyName}
              </motion.h1>
              <motion.h4
                initial={{ opacity: 0, x: -200 }}
                animate={{ opacity: 0.8, x: 0 }}
                transition={{ delay: 2.5, duration: 1 }}
                className="theme-blue text-font p-text-right sub-company-text"
              >
                {companyContents.companyTag}
              </motion.h4>
  </div>**/}
            {/**<div className="p-field p-col-12 p-md-4" id="logo-container">
              <motion.img
                initial={{ opacity: 0, y: -200 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ delay: 4, duration: 1 }}
                src="./image-container/DS-Logo2.png"
                className="company-logo-style"
              ></motion.img>
  </div>**/}
            <div className="p-field p-col-12 p-md-8">
              <figure className="display-end">
                <img
                          data-aos="fade-in"
                          data-aos-offset="300"
                          data-aos-delay="1500"
                          data-aos-duration="2000"
                          data-aos-mirror="true"
                  src="./image-container/head-text.png"
                  className="element-image-style-head"
                ></img>
              </figure>
            </div>
          </div>
        </div>

        {/**<div className="about-us">
          <h2
            style={this.headerText}
            className="theme-white highlight-font p-text-center"
          >
            {aboutContents.title}
          </h2>
          <p className="theme-white text-font-two content-text p-text-center p-pb-5">
            {aboutContents.content}
          </p>
          </div>**/}
        <div className="main">
          <div className="core">
            <h2
              data-aos="fade-right"
              data-aos-offset="200"
              data-aos-delay="50"
              data-aos-duration="1000"
              style={this.headerTextTwo}
              className="theme-black highlight-font"
            >
              {core.Title}
              <hr align="left" className="headHrStyle p-mt-2  p-mb-4"></hr>
            </h2>

            <div className="p-fluid p-grid p-mt-6 cards-part">
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
                  data-aos="fade-right"
                  data-aos-offset="300"
                  data-aos-delay="50"
                  data-aos-duration="1000"
                >
                  <Card
                    title="We Create"
                    header={cardHeader}
                    className="card-hover"
                  >
                    {servicesContent.cardContentOne}
                  </Card>
                </div>
              </div>
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
                  data-aos="fade-up"
                  data-aos-offset="400"
                  data-aos-delay="100"
                  data-aos-duration="1000"
                >
                  <Card
                    title="We Innovate"
                    header={cardHeaderTwo}
                    className="card-hover"
                  >
                    {servicesContent.cardContentTwo}
                  </Card>
                </div>
              </div>
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
                  data-aos="fade-left"
                  data-aos-offset="500"
                  data-aos-delay="150"
                  data-aos-duration="1000"
                >
                  <Card
                    title="We Connect"
                    header={cardHeaderThree}
                    className="card-hover"
                  >
                    {servicesContent.cardContentThree}
                  </Card>
                </div>
              </div>
            </div>
          </div>

          <div className="technologies">
            <h2
              data-aos="fade-right"
              data-aos-offset="200"
              data-aos-delay="50"
              data-aos-duration="1000"
              style={this.headerTextTwo}
              className="theme-black highlight-font p-mb-0"
            >
              {tech.Title}
              <hr align="left" className="headHrStyle p-mt-2 p-mb-6"></hr>
            </h2>
            <div className="tech-part">
            <div className="p-fluid p-grid p-mt-6"
                   data-aos="fade-right"
                   data-aos-offset="300"
                   data-aos-delay="50"
                   data-aos-duration="1000">
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
                >
                  <img
                  src="./image-container/html.png"
                  className="tech-style is-centered"
                ></img>
                </div>
              </div>
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
                >
                  <img
                  src="./image-container/css.png"
                  className="tech-style-two is-centered"
                ></img>
                </div>
              </div>
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
                >
                   <img
                  src="./image-container/js.png"
                  className="tech-style-three is-centered"
                ></img>
                </div>
              </div>
            </div>
            <div className="p-fluid p-grid p-mt-5 tech-part"
                             data-aos="fade-left"
                             data-aos-offset="300"
                             data-aos-delay="50"
                             data-aos-duration="1000">
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
 
                >
                  <img
                  src="./image-container/react.png"
                  className="tech-style-four is-centered"
                ></img>
                </div>
              </div>
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
                >
                  <img
                  src="./image-container/vue.png"
                  className="tech-style-two is-centered"
                ></img>
                </div>
              </div>
              <div className="p-field p-col-12 p-md-4">
                <div
                  className="p-field p-col-12 p-md-10"
                >
                   <img
                  src="./image-container/sql.png"
                  className="tech-style is-centered"
                ></img>
                </div>
              </div>
            </div>
            </div>
          </div>

          {/**<div className="services">
          <h2
            style={this.headerTextTwo}
            className="theme-white highlight-font p-text-center p-pb-4"
          >
            {servicesContent.title}
          </h2>
          <div className="p-fluid p-grid">
            <div className="p-field p-col-12 p-md-4">
              <div className="p-field p-col-12 p-md-10">
                <Card
                  title="Responsive Web Development"
                  subTitle="SubTitle"
                  header={cardHeader}
                >
                  {servicesContent.cardContentOne}
                </Card>
              </div>
            </div>
            <div className="p-field p-col-12 p-md-4">
              <div className="p-field p-col-12 p-md-10">
                <Card
                  title="Applications Development"
                  subTitle="SubTitle"
                  header={cardHeaderTwo}
                >
                  {servicesContent.cardContentTwo}
                </Card>
              </div>
            </div>
            <div className="p-field p-col-12 p-md-4">
              <div className="p-field p-col-12 p-md-10">
                <Card
                  title="Secured and Quality Service"
                  subTitle="SubTitle"
                  header={cardHeaderThree}
                >
                  {servicesContent.cardContentThree}
                </Card>
              </div>
            </div>
          </div>
        </div> **/}

          {/** <div className="recent-works">
          <h2
            style={this.headerText}
            className="theme-white highlight-font p-text-center p-pb-4"
          >
            {projectContent.title}
          </h2>
          <div className="p-d-flex p-flex-column p-flex-md-row">
            <div className="p-mb-3">
              <h2 style={this.titleText} className="theme-white highlight-font">
                {projectContent.projectTitle}
              </h2>
              <p className="theme-white text-font-two project-text p-mr-5">
                {projectContent.projectDesc}
              </p>
              <Button
                label="Live Demo"
                icon="pi pi-eye"
                iconPos="left"
                className="p-button-danger p-button-large"
              />
            </div>
            <div className="p-mb-3">{projImage}</div>
          </div>
          <div className="p-d-flex p-flex-column p-flex-md-row">
            <div className="p-mb-3">
              <h2 style={this.titleText} className="theme-white highlight-font">
                {projectContent.projectTitleTwo}
              </h2>
              <p className="theme-white text-font-two project-text p-mr-5">
                {projectContent.projectDescTwo}
              </p>
              <Button
                label="Live Demo"
                icon="pi pi-eye"
                iconPos="left"
                className="p-button-danger p-button-large"
              />
            </div>
            <div className="p-mb-3">{projImageTwo}</div>
          </div>
          <div className="p-d-flex p-flex-column p-flex-md-row">
            <div className="p-mb-3">
              <h2 style={this.titleText} className="theme-white highlight-font">
                {projectContent.projectTitleThree}
              </h2>
              <p className="theme-white text-font-two project-text p-mr-5">
                {projectContent.projectDescThree}
              </p>
              <Button
                label="Live Demo"
                icon="pi pi-eye"
                iconPos="left"
                className="p-button-danger p-button-large"
              />
            </div>
            <div className="p-mb-6">{projImageThree}</div>
          </div>
        </div> **/}

          <div className="contact-us">
            <h2
              data-aos="fade-right"
              data-aos-offset="200"
              data-aos-delay="50"
              data-aos-duration="1000"
              style={this.headerTextTwo}
              className="theme-black highlight-font p-mb-0"
            >
              {contactContent.title}
              <hr align="left" className="headHrStyle p-mt-2 p-mb-4"></hr>
            </h2>
            <h3
              data-aos="fade-up"
              data-aos-offset="200"
              data-aos-delay="50"
              data-aos-duration="1000"
              style={this.contactText}
              className="theme-black text-font-two p-text-center p-mt-6"
            >
              {contactContent.content}
              <span style={this.contactText} className="theme-red">
                {contactContent.subContent}
              </span>
            </h3>
            <div
              data-aos="fade-right"
              data-aos-offset="300"
              data-aos-delay="100"
              data-aos-duration="1000"
              className="centered-button"
            >
              <LinkButton to="/contact" className="btn btn-primary btn-lg">
                <b>
                  <i className="pi pi-send p-mr-2"></i>Send us a Message!
                </b>
              </LinkButton>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Body;
