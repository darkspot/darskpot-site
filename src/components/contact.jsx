import React, { Component } from "react";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import LinkButton from "./LinkButton";
import { InputText } from "primereact/inputtext";
import { Form } from "react-final-form";
import { InputTextarea } from "primereact/inputtextarea";
import { Field } from "react-final-form-html5-validation";

const MyForm = () => {
  return (
    <Form
      onSubmit={(formObj) => {
        console.log(formObj);
      }}
    >
      {({ handleSubmit, pristine, invalid }) => (
        <Card>
          <form onSubmit={handleSubmit}>
            <div className="p-fluid p-grid p-mb-2">
              <div className="p-field p-col-12 p-md-6">
                <Field name="fname">
                  {({ input }) => (
                    <span className="p-float-label">
                      <InputText
                        name="fname"
                        {...input}
                        required
                        maxLength={15}
                        tooLong="Your name is too long."
                        minLength={3}
                        tooShort="Your name is too short."
                      />
                      <label>Firstname</label>
                    </span>
                  )}
                </Field>
              </div>
              <div className="p-field p-col-12 p-md-6">
                <Field name="lname">
                  {({ input }) => (
                    <span className="p-float-label">
                      <InputText
                        name="lname"
                        {...input}
                        required
                        maxLength={12}
                        tooLong="Your lastname is too long."
                        minLength={3}
                        tooShort="Your lastname is too short."
                      />
                      <label>Lastname</label>
                    </span>
                  )}
                </Field>
              </div>
            </div>
            <Field name="email">
              {({ input }) => (
                <span className="p-float-label p-mb-5">
                  <InputText
                    name="email"
                    {...input}
                    required
                    pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{2,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"
                    patternMismatch="This is not a valid email!"
                  />
                  <label>Email</label>
                </span>
              )}
            </Field>
            <Field name="contact">
              {({ input }) => (
                <span className="p-float-label p-mb-5">
                  <InputText
                    name="contact"
                    {...input}
                    required
                    minLength={5}
                    maxLength={15}
                    pattern="^(09|\+639)\d{9}$"
                  />
                  <label>Contact Number</label>
                </span>
              )}
            </Field>
            <Field name="message">
              {({ input }) => (
                <span className="p-float-label p-mb-5">
                  <InputTextarea
                    name="message"
                    rows={8}
                    {...input}
                    required
                    maxLength={350}
                    minLength={20}
                  />
                  <label>Message</label>
                </span>
              )}
            </Field>
            <Button
              label="Send Message"
              type="submit"
              className="p-button-danger p-button-lg p-mt-0"
            />
          </form>
        </Card>
      )}
    </Form>
  );
};

const pageContent = {
  preTitle: "GOT A QUESTION?",
  title: "Contact Darkspot",
  content:
    "We're here to help and answer your questions and queries you might have. We look forward in hearing and working with you.",
};

class Contact extends Component {
  state = {};

  mainText = {
    fontSize: 40,
    marginTop: 0,
  };

  subText = {
    fontSize: 25,
    marginBottom: 0,
  };

  render() {
    return (
      <React.Fragment>
        <div className="contact-container">
          <LinkButton to="/" className="btn btn-primary centered-form">
            <i
              className="pi pi-chevron-left p-mr-1 p-mt-1"
              style={{ fontSize: "1.2em" }}
            ></i>
          </LinkButton>
          <div className="p-fluid p-grid centered-form">
            <div className="p-field p-col-12 p-md-4 p-mt-6">
              <div className="p-field p-col-12 p-md-10">
                <h2 style={this.subText} className="text-font-two theme-red">
                  {pageContent.preTitle}
                </h2>
                <h1
                  style={this.mainText}
                  className="highlight-font theme-black"
                >
                  {pageContent.title}
                </h1>
                <h3 className="text-font-two theme-black">
                  {pageContent.content}
                </h3>
              </div>
            </div>

            <div className="p-field p-col-12 p-md-6 p-mt-6 centered-form">
              <MyForm />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Contact;
