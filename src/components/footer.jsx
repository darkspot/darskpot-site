import React, { Component } from "react";

const social = {
  github: "pi pi-github theme-blue p-mr-3 p-ml-3",
  facebook: "pi pi-facebook theme-blue p-mr-3 p-ml-3",
  google: "pi pi-google theme-blue p-mr-3 p-ml-3",
  twitter: "pi pi-twitter theme-blue p-mr-3 p-ml-3",
  up: "pi pi-chevron-up theme-light-orange",
};

const scrollTop = () => {
  window.scrollTo({ top: 0, behavior: "smooth" });
};

class Footer extends Component {
  state = {
    footerContent: "2021 - Darkspot IT Solutions.",
  };

  iconSize = {
    fontSize: 35,
    paddingTop: 50,
  };

  upSize = {
    fontSize: 35,
  };

  render() {
    return (
      <React.Fragment>
        <div className="footer">
          <div className="centered-form">
            <i
              style={this.upSize}
              className="pi pi-chevron-up theme-red up-arrow"
              onClick={scrollTop}
            ></i>
          </div>
          <div className="social-links">
            <i style={this.iconSize} className={social.github}></i>
            <i style={this.iconSize} className={social.facebook}></i>
            <i style={this.iconSize} className={social.google}></i>
            <i style={this.iconSize} className={social.twitter}></i>
          </div>
          <hr className="divider-style" />
          <h4 className="footer-text text-font p-text-center theme-light-grey">
            &copy; {this.state.footerContent}
          </h4>
        </div>
      </React.Fragment>
    );
  }
}

export default Footer;
